# Asciidoctor glossary Extension
:version: 0.0.2

`@djencks/asciidoctor-glossary` provides an Asciidoctor.js extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-glossary/-/blob/master/README.adoc.

## Installation

Available through npm as @djencks/asciidoctor-glossary.

The project git repository is https://gitlab.com/djencks/asciidoctor-glossary

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-glossary/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-glossary/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension is under extensions/glossary-extension in https://gitlab.com/djencks/simple-examples.
