/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const glossary = require('./../lib/glossary')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

describe('glossary tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const contentCatalog = mockContentCatalog(seed)

    const vfss = [
      // {
      //   vfsType: 'plain',
      //   config: {
      //     vfs: {
      //       read: (filespec) => {
      //         filespec = filespec.substring(filespec.lastIndexOf(':') + 1).substring(filespec.lastIndexOf('$') + 1)
      //         return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
      //           accum.contents = Buffer.from(f.contents)
      //           return accum
      //         }, {}).contents
      //       },
      //
      //     },
      //   },
      // },
      {
        vfsType: 'antora',
        config: (id, attributes = {}) => {
          return {
            file: { src: seed[id], contents: seed[id].contents },
            contentCatalog,
            config: { attributes },
          }
        },
      },
    ]
    return vfss
  }

  ;[
  //   {
  //   type: 'global',
  //   f: (text, config) => {
  //     glossary.register(asciidoctor.Extensions, config)
  //     return asciidoctor.load(text)
  //   },
  // },
    {
      type: 'registry',
      f: (config) => {
        asciidoctor.Extensions.unregisterAll()
        const registry = glossary.register(asciidoctor.Extensions.create(), config)
        return asciidoctor.load(config.file.contents,
          { extension_registry: registry, attributes: config.config.attributes })
      },
    },
  ].forEach(({ type, f }) => {
    prepareVfss([
      {
        version: '4.5',
        component: 'component-a',
        family: 'page',
        relative: 'page-a.adoc',
        contents: `= The Glossary

We might drink glossterm:whiskey[a product of Scotland] when solving bugs.

This is a sentence with the term glossterm:infinitude[a whole lot] to be defined.
This is a sentence with the term glossterm:unlikely[apt to not occur] used appropriately.

When the glossterm:whiskey[] is all gone, we can switch to gin.

Finally, in another paragraph we consider the glossterm:jabberwock[a poetical beast].

`,
      },
      {
        version: '4.5',
        component: 'component-a',
        family: 'page',
        relative: 'glossary.adoc',
        contents: `= Glossary

glossary::[]

`,
      },
      {
        version: '4.5',
        component: 'component-b',
        family: 'page',
        relative: 'glossary.adoc',
        contents: `= The Glossary

We might drink glossterm:usquebaugh[a product of the Highlands] when solving bugs.

This is a sentence with the term glossterm:negligible[not a whole lot] to be defined.
This is a sentence with the term glossterm:likely[not apt to not occur] used appropriately.

When the glossterm:usquebaugh[] is all gone, we can switch to armagnac.

Finally, in another paragraph we consider the glossterm:mome rath[an outgraber].

== Glossary

glossary::[]

`,
      },
    ]).forEach(({ vfsType, config }) => {
      it(`separate glossary test, ${type}, ${vfsType}`, () => {
        const attributes = { 'glossary-page': 'foo-glossary.adoc' }
        const doc0 = f(config(0, attributes))
        const html0 = doc0.convert()
        expect(html0).to.equal(`<div class="paragraph">
<p>We might drink <a href="foo-glossary.html#_glossterm_whiskey" class="glossary-term">whiskey</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a href="foo-glossary.html#_glossterm_infinitude" class="glossary-term">infinitude</a> to be defined.
This is a sentence with the term <a href="foo-glossary.html#_glossterm_unlikely" class="glossary-term">unlikely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the <a href="foo-glossary.html#_glossterm_whiskey" class="glossary-term">whiskey</a> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a href="foo-glossary.html#_glossterm_jabberwock" class="glossary-term">jabberwock</a>.</p>
</div>`)
        const doc1 = f(config(1, attributes))
        const html1 = doc1.convert()
        expect(html1).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>`)
        const doc2 = f(config(2))
        const html2 = doc2.convert()
        expect(html2).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <a href="#_glossterm_usquebaugh" class="glossary-term">usquebaugh</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a href="#_glossterm_negligible" class="glossary-term">negligible</a> to be defined.
This is a sentence with the term <a href="#_glossterm_likely" class="glossary-term">likely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the <a href="#_glossterm_usquebaugh" class="glossary-term">usquebaugh</a> is all gone, we can switch to armagnac.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a href="#_glossterm_mome_rath" class="glossary-term">mome rath</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_likely"></a>likely</dt>
<dd>
<p>not apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_mome_rath"></a>mome rath</dt>
<dd>
<p>an outgraber</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_negligible"></a>negligible</dt>
<dd>
<p>not a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_usquebaugh"></a>usquebaugh</dt>
<dd>
<p>a product of the Highlands</p>
</dd>
</dl>
</div>
</div>
</div>`)
      })
    })
  })
})
