/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const glossary = require('./../lib/glossary')
const { expect } = require('chai')
const stdout = require('test-console').stdout

const page1 = `= The Glossary

We might drink glossterm:whiskey[a product of Scotland] when solving bugs.

This is a sentence with the term glossterm:infinitude[a whole lot] to be defined.
This is a sentence with the term glossterm:unlikely[apt to not occur] used appropriately.

When the selection of glossterm:whiskeys[term=whiskey] is all gone, we can switch to gin.

Finally, in another paragraph we consider the glossterm:jabberwock[a poetical beast].

== Glossary

glossary::[]

`
describe('glossary tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  ;[{
    type: 'global',
    f: (text, attributes = {}) => {
      glossary.register(asciidoctor.Extensions)
      return asciidoctor.load(text, { attributes })
    },
  },
  {
    type: 'registry',
    f: (text, attributes = {}) => {
      const registry = glossary.register(asciidoctor.Extensions.create())
      return asciidoctor.load(text, { extension_registry: registry, attributes })
    },
  }].forEach(({ type, f }) => {
    it(`glossary test, default, ${type}`, () => {
      const doc = f(page1)
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <a href="#_glossterm_whiskey" class="glossary-term">whiskey</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a href="#_glossterm_infinitude" class="glossary-term">infinitude</a> to be defined.
This is a sentence with the term <a href="#_glossterm_unlikely" class="glossary-term">unlikely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <a href="#_glossterm_whiskey" class="glossary-term">whiskeys</a> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a href="#_glossterm_jabberwock" class="glossary-term">jabberwock</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, explicit role, ${type}`, () => {
      const doc = f(page1, { 'glossary-term-role': 'custom-glossterm-role' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <a href="#_glossterm_whiskey" class="custom-glossterm-role">whiskey</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a href="#_glossterm_infinitude" class="custom-glossterm-role">infinitude</a> to be defined.
This is a sentence with the term <a href="#_glossterm_unlikely" class="custom-glossterm-role">unlikely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <a href="#_glossterm_whiskey" class="custom-glossterm-role">whiskeys</a> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a href="#_glossterm_jabberwock" class="custom-glossterm-role">jabberwock</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, explicit role, tooltip default (true), ${type}`, () => {
      const doc = f(page1, { 'glossary-term-role': 'custom-glossterm-role', 'glossary-tooltip': 'true' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <a data-glossary-tooltip="a product of Scotland" href="#_glossterm_whiskey" class="custom-glossterm-role">whiskey</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a data-glossary-tooltip="a whole lot" href="#_glossterm_infinitude" class="custom-glossterm-role">infinitude</a> to be defined.
This is a sentence with the term <a data-glossary-tooltip="apt to not occur" href="#_glossterm_unlikely" class="custom-glossterm-role">unlikely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <a data-glossary-tooltip="a product of Scotland" href="#_glossterm_whiskey" class="custom-glossterm-role">whiskeys</a> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a data-glossary-tooltip="a poetical beast" href="#_glossterm_jabberwock" class="custom-glossterm-role">jabberwock</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, no glossterm role, ${type}`, () => {
      const doc = f(page1, { 'glossary-term-role': '' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <a href="#_glossterm_whiskey">whiskey</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a href="#_glossterm_infinitude">infinitude</a> to be defined.
This is a sentence with the term <a href="#_glossterm_unlikely">unlikely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <a href="#_glossterm_whiskey">whiskeys</a> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a href="#_glossterm_jabberwock">jabberwock</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, no links, ${type}`, () => {
      const doc = f(page1, { 'glossary-links': 'false' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <span class="glossary-term">whiskey</span> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <span class="glossary-term">infinitude</span> to be defined.
This is a sentence with the term <span class="glossary-term">unlikely</span> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <span class="glossary-term">whiskeys</span> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <span class="glossary-term">jabberwock</span>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, no links, no glossterm role, ${type}`, () => {
      const doc = f(page1, { 'glossary-links': 'false', 'glossary-term-role': '' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink whiskey when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term infinitude to be defined.
This is a sentence with the term unlikely used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of whiskeys is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the jabberwock.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, no links, no glossterm role, tooltip title, ${type}`, () => {
      const doc = f(page1, { 'glossary-links': 'false', 'glossary-term-role': '', 'glossary-tooltip': 'title' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <span title="a product of Scotland">whiskey</span> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <span title="a whole lot">infinitude</span> to be defined.
This is a sentence with the term <span title="apt to not occur">unlikely</span> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <span title="a product of Scotland">whiskeys</span> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <span title="a poetical beast">jabberwock</span>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, no links, no glossterm role, tooltip default (true), ${type}`, () => {
      const doc = f(page1, { 'glossary-links': 'false', 'glossary-term-role': '', 'glossary-tooltip': 'true' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <span data-glossary-tooltip="a product of Scotland">whiskey</span> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <span data-glossary-tooltip="a whole lot">infinitude</span> to be defined.
This is a sentence with the term <span data-glossary-tooltip="apt to not occur">unlikely</span> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <span data-glossary-tooltip="a product of Scotland">whiskeys</span> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <span data-glossary-tooltip="a poetical beast">jabberwock</span>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, no links, no glossterm role, tooltip custom, ${type}`, () => {
      const doc = f(page1, { 'glossary-links': 'false', 'glossary-term-role': '', 'glossary-tooltip': 'data-foo-tootltip' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <span data-foo-tootltip="a product of Scotland">whiskey</span> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <span data-foo-tootltip="a whole lot">infinitude</span> to be defined.
This is a sentence with the term <span data-foo-tootltip="apt to not occur">unlikely</span> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <span data-foo-tootltip="a product of Scotland">whiskeys</span> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <span data-foo-tootltip="a poetical beast">jabberwock</span>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, specified glossary page, ${type}`, () => {
      const doc = f(page1, { 'glossary-page': 'foo-glossary.adoc' })
      const html = doc.convert()
      expect(html).to.equal(`<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>We might drink <a href="foo-glossary.html#_glossterm_whiskey" class="glossary-term">whiskey</a> when solving bugs.</p>
</div>
<div class="paragraph">
<p>This is a sentence with the term <a href="foo-glossary.html#_glossterm_infinitude" class="glossary-term">infinitude</a> to be defined.
This is a sentence with the term <a href="foo-glossary.html#_glossterm_unlikely" class="glossary-term">unlikely</a> used appropriately.</p>
</div>
<div class="paragraph">
<p>When the selection of <a href="foo-glossary.html#_glossterm_whiskey" class="glossary-term">whiskeys</a> is all gone, we can switch to gin.</p>
</div>
<div class="paragraph">
<p>Finally, in another paragraph we consider the <a href="foo-glossary.html#_glossterm_jabberwock" class="glossary-term">jabberwock</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_glossary">Glossary</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="_glossterm_infinitude"></a>infinitude</dt>
<dd>
<p>a whole lot</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_jabberwock"></a>jabberwock</dt>
<dd>
<p>a poetical beast</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_unlikely"></a>unlikely</dt>
<dd>
<p>apt to not occur</p>
</dd>
<dt class="hdlist1"><a id="_glossterm_whiskey"></a>whiskey</dt>
<dd>
<p>a product of Scotland</p>
</dd>
</dl>
</div>
</div>
</div>`)
    })

    it(`glossary test, default, no console output, ${type}`, () => {
      const inspect = stdout.inspect()
      const doc = f(page1)
      doc.convert()
      inspect.restore()
      expect(inspect.output).to.deep.equal([])
    })

    it(`glossary test, default, console output, ${type}`, () => {
      const inspect = stdout.inspect()
      const doc = f(page1, { 'glossary-log-terms': 'true' })
      doc.convert()
      inspect.restore()
      expect(inspect.output).to.deep.equal([
        'whiskey::  a product of Scotland\n',
        'infinitude::  a whole lot\n',
        'unlikely::  apt to not occur\n',
        'jabberwock::  a poetical beast\n',
      ])
    })
  })
})
